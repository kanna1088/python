#!/bin/python
#-*- coding:utf-8 -*-

import random

# 初期化
age = 0

# 関数定義
def ans():
    print(str(age) + "歳、" + who + "です。")

#本文
while age != 24:
    age = random.randint(1,117)
    if age == 24:
        who = "学生"
        ans()
    elif age == 19:
        who = "成人（予定)"
        ans()
    elif age == 60:
        who = "還暦"
        ans()
    elif age == 70:
        who = "古希"
        ans()
    elif age == 77:
        who = "喜寿"
        ans()
    elif age == 80:
        who = "傘寿"
        ans()
    elif age == 88:
        who = "米寿"
        ans()
    elif age == 90:
        who = "卒寿"
        ans()
    elif age == 99:
        who = "白寿"
        ans()
    elif age == 100:
        who = "百寿"
        ans()
    elif age == 117:
        who = "最高齢（2017年現在)"
    elif age <= 6:
        who = "幼児"
        ans()
    elif age <= 12:
        who = "小学生"
        ans()
    elif age <= 15:
        who = "中学生"
        ans()
    elif age <= 18:
        who = "高校生"
        ans()
    elif age >= 20:
        who = "成人"
        ans()
